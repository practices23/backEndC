<?php

namespace Crimsoncircle\Controller;
include_once "Conexion.php";

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController
{

    public function create(Request $request )
    {
        
        $method = $request->getMethod();
        $data =  $request -> getContent();

        if($method === 'POST'){
            return $data;
        }
        return "seleccione el metodo post";

    }

    public function commentById(Request $request )
    {
        $method = $request->getMethod();
        $data =  $request -> getContent();
        if($method === 'GET'){
        
            return 'the method is get!';
        }elseif($method === 'DELETE'){
            return 'the method is delete!';

        }else{
            return 'the method is not accept!';

        }

    }

    public function getByPostId(Request $request )
    {
        
            return 'Yep, this is a leap year!';

    }

}