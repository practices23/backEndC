<?php
use Symfony\Component\Routing;

$routes = new Routing\RouteCollection();
$routes->add('leap_year', new Routing\Route('/is_leap_year/{year}', [
    'year' => null,
    '_controller' => 'Crimsoncircle\Controller\LeapYearController::index',
]));

$routes->add('blog_post', new Routing\Route('/blog', array(
    '_controller' => 'Crimsoncircle\Controller\BlogController::create',
), array(
    '_method' => 'POST',
)));

$routes->add('blogBySlug', new Routing\Route('/blog/{slug}',[
    '_controller' => 'Crimsoncircle\Controller\BlogController::blogBySlug',
    '_methods'=> ['GET','DELETE']
]));

$routes->add('comment', new Routing\Route('/comment',[
    '_controller' => 'Crimsoncircle\Controller\CommentController::create',
    '_methods'=> 'POST'
]));

$routes->add('commentById', new Routing\Route('/comment/{id}',[
    '_controller' => 'Crimsoncircle\Controller\CommentController::commentById',
    '_methods'=> ['GET','DELETE']
]));


$routes->add('getCommentByPostId', new Routing\Route('/blog/post/{id}',[
    '_controller' => 'Crimsoncircle\Controller\CommentController::getByPostId',
    '_methods'=> 'GET'
]));

return $routes;

