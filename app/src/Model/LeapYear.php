<?php

namespace Crimsoncircle\Model;

class LeapYear
{
    public function isLeapYear($year)
    {
        $year = $year;

        return !($year % 4) && ($year %100 || !($year % 400));

        //TODO: Logic must be implemented to calculate if a year is a leap year

    }
}